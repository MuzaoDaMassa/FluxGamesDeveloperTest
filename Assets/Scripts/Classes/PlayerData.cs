using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;

[System.Serializable]
public class PlayerData
{
    public int _level;
    public int _score;

    public PlayerData (ControlGame _controlGame)
    {
        _level = Statistics.CurrentLevel;
        _score = Statistics.Points;
    }

}
