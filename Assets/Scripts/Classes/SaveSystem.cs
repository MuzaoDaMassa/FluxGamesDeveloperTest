using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class SaveSystem
{
    public static void SavePlayer(ControlGame _controlGame)
    {
        BinaryFormatter _formatter = new BinaryFormatter();
        string _path = Application.persistentDataPath + "/player.SaveData";
        FileStream _stream = new FileStream(_path, FileMode.Create);

        PlayerData _data = new PlayerData(_controlGame);

        _formatter.Serialize(_stream, _data);
        _stream.Close();
    }

    public static PlayerData LoadPlayer()
    {
        string _path = Application.persistentDataPath + "/player.SaveData";
        if (File.Exists(_path))
        {
            BinaryFormatter _formatter = new BinaryFormatter();
            FileStream _stream = new FileStream(_path, FileMode.Open);

            PlayerData _data = _formatter.Deserialize(_stream) as PlayerData;
            _stream.Close();

            return _data;
        }
        else
        {
            Debug.Log("Save file not found in " + _path);
            return null;
        }
    }
}
