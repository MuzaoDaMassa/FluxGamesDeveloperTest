﻿using Gaminho;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ControlGame : MonoBehaviour {
    public ScenarioLimits ScenarioLimit;
    public Level[] Levels;
    public Image Background;
    [Header("UI")]
    public Text TextStart;
    public Text TextPoints;
    public Transform BarLife;
    public int _currentLevel, _points;

    private void Awake()
    {
        if (SceneManager.GetActiveScene().name == "Continue Game Scene")
        {
            LoadPlayerData();
            Debug.Log(Statistics.CurrentLevel);
        }
    }

    // Use this for initialization
    void Start ()
    {
        _currentLevel = Statistics.CurrentLevel;
        Debug.Log(_currentLevel);
        Time.timeScale = 1.0f;
        Statistics.EnemiesDead = 0;
        Background.sprite = Levels[_currentLevel].Background;
        TextStart.text = "Stage " + (Statistics.CurrentLevel + 1);
        GetComponent<AudioSource>().PlayOneShot(Levels[_currentLevel].AudioLvl);

    }

    private void Update()
    {
        bool witdShield = Statistics.WithShield;
        int enemiesDead = Statistics.EnemiesDead;
        _currentLevel = Statistics.CurrentLevel;
        _points = Statistics.Points;
        int shootingSelected = Statistics.ShootingSelected;

        TextPoints.text = _points.ToString();
        BarLife.localScale = new Vector3(Statistics.Life / 10f, 1, 1);
    }

    public void LevelPassed()
    {
        Clear();
        if (Statistics.CurrentLevel <= 2)
        {
            Statistics.CurrentLevel++;
        }
        Statistics.Points += 1000 * (Statistics.CurrentLevel);
        if (Statistics.CurrentLevel < 3)
        {
            GameObject.Instantiate(Resources.Load(Statistics.PREFAB_LEVELUP) as GameObject);
        }
        else
        {
            GameObject.Instantiate(Resources.Load(Statistics.PREFAB_CONGRATULATION) as GameObject);
        }
    }

    //Oops, when you lose (: Starts from Zero
    public void GameOver()
    {
        BarLife.localScale = new Vector3(0, 1, 1);
        Clear();
        Destroy(Statistics.Player.gameObject);
        GameObject.Instantiate(Resources.Load(Statistics.PREFAB_GAMEOVER) as GameObject);
        Time.timeScale = 0.0f;
    }
    private void Clear()
    {
        //Statistics.CurrentLevel++;
        SavePlayerData();
        GetComponent<AudioSource>().Stop();
        GameObject[] Enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject ini in Enemies)
        {
            Destroy(ini);
        }
    }

    public void SavePlayerData()
    {
        SaveSystem.SavePlayer(this);
    }

    public void LoadPlayerData()
    {
        PlayerData _data = SaveSystem.LoadPlayer();

        Statistics.CurrentLevel = _data._level;
        Statistics.Points = _data._score;
    }
}
