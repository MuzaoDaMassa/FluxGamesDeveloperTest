﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RecordControl : MonoBehaviour {
    public Text TxtRecord;
    public InputField InputRecord;
	// Use this for initialization
	void Start () {
        InputRecord.text = PlayerPrefs.GetString(Statistics.PLAYERPREF_NEWRECORD);
        InputRecord.interactable = false;
        if (Statistics.Points > PlayerPrefs.GetInt(Statistics.PLAYERPREF_VALUE))
        {
            PlayerPrefs.SetInt(Statistics.PLAYERPREF_VALUE, Statistics.Points);
            InputRecord.interactable = true;
            InputRecord.text = "";
        }

        TxtRecord.text = "Your Points: " + Statistics.Points+"\nRecord: " + PlayerPrefs.GetInt(Statistics.PLAYERPREF_VALUE).ToString();

    }
	
	public void UpdateName()
    {
        PlayerPrefs.SetString(Statistics.PLAYERPREF_NEWRECORD, InputRecord.text);
    }
}
