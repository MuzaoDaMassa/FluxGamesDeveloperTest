﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ControlStart : MonoBehaviour {
    public Text Record;
    public Button button;
	// Use this for initialization
	void Start () {
        Time.timeScale = 1.0f;

        //Loads Record
        if (PlayerPrefs.GetInt(Statistics.PLAYERPREF_VALUE) == 0)
        {
            PlayerPrefs.SetString(Statistics.PLAYERPREF_NEWRECORD, "Nobody");
        }
        Record.text = "Record: " + PlayerPrefs.GetString(Statistics.PLAYERPREF_NEWRECORD) + "(" + PlayerPrefs.GetInt(Statistics.PLAYERPREF_VALUE) + ")";
	}

	public void StartClick()
    {
        Time.timeScale = 1.0f;
        Statistics.WithShield = false;
        Statistics.EnemiesDead = 0;
        Statistics.CurrentLevel = 0;
        Statistics.Points = 0;
        Statistics.ShootingSelected = 2;

        GetComponent<AudioSource>().Stop();
        GameObject.Instantiate(Resources.Load(Statistics.PREFAB_HISTORY) as GameObject);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void CotinueButton()
    {
        SceneManager.LoadScene("Continue Game Scene");
    }

    
}
