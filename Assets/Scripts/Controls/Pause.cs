using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    private static bool _isGamePaused = false;
    private AudioListener _camAudioListener;

    public GameObject _pauseMenu;
    public GameObject _mainCamera;

    private void Start()
    {
        _camAudioListener = _mainCamera.GetComponent<AudioListener>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            if (_isGamePaused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    private void ResumeGame()
    {
        _isGamePaused = false;
        _camAudioListener.enabled = true;
        Time.timeScale = 1.0f;
        _pauseMenu.SetActive(false);
    }

    void PauseGame()
    {
        _isGamePaused = true;
        _camAudioListener.enabled = false;
        _pauseMenu.SetActive(true);
        Time.timeScale = 0.0f;
    }
}
