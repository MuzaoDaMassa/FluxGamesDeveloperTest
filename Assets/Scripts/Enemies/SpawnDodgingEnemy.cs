using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDodgingEnemy : MonoBehaviour
{
    public GameObject _enemy, _enemyContainer;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(SpawnEnemyRoutine());
    }

    IEnumerator SpawnEnemyRoutine()
    {
        float _spawnRate = Random.Range(2.0f, 5.0f);
        yield return new WaitForSeconds(_spawnRate);

        Vector2 _posToSpawn = new Vector2(695, 380);
        GameObject _newEnemy = Instantiate(_enemy, _posToSpawn, Quaternion.identity);
        _newEnemy.transform.parent = _enemyContainer.transform;

        StartCoroutine(SpawnEnemyRoutine());
    }
}
