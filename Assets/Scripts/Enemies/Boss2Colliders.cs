using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss2Colliders : MonoBehaviour
{
    public Boss2 _boss2;
    // public Life _lifeScript;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Shot")
        {
            Destroy(other.gameObject);
            _boss2.KillMe(this.gameObject);
            
        }
    }
}
