﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Gaminho;
using System;

public class Enemy : MonoBehaviour {

    public int Damage = 1;
    public float _dodgeSpeed = 1000f;
    public bool _dodgeAbility = true;
    public bool normal = true;
    public Shot shot;
    public Life _life;
    public GameObject ItemDrop;
    public GameObject Explosion;
    public Statistics.TYPE_ENEMY MyType;

    // Use this for initialization
    void Start () {
        if (MyType == Statistics.TYPE_ENEMY.SHIP)
        {
            StartCoroutine(Shoot());
        }
    }

    private void Update()
    {
        if (!Statistics.Player) return;
        if(MyType == Statistics.TYPE_ENEMY.SHIP)
        {
            Quaternion q = Statistics.FaceObject(Statistics.Player.localPosition, transform.localPosition, Statistics.FacingDirection.Right);
            transform.rotation = q;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        
        if (other.gameObject.tag == "Shot")
        {

            Destroy(other.gameObject);
            StartCoroutine(_life.TakesLife(Statistics.Damage));
            Instantiate(Explosion, transform).transform.parent = transform.parent;
            
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Shot2(Clone)" || other.gameObject.name == "Shot3(Clone)")
        {
            
            transform.position = Vector2.MoveTowards(transform.position, Vector2.Perpendicular(new Vector2 (other.gameObject.transform.position.x, other.gameObject.transform.position.y)) , 75 *_dodgeSpeed * Time.deltaTime);
        }
    }


    public void MyDeath()
    {
        //Explosao;
        
        if(MyType == Statistics.TYPE_ENEMY.METEOR)
        {
            
            if (UnityEngine.Random.Range(0, 100) > 60)//60% chance of it becoming bits and pieces
            {
                Create(1);
                Create(2);
                Create(3);
                Create(4);
            }
            Statistics.EnemiesDead++;
        }

        if (ItemDrop != null && UnityEngine.Random.Range(0, 100) > 70)
        {
            Instantiate(ItemDrop, transform.parent).transform.localPosition = transform.localPosition;
        }

        Statistics.Points += Damage * 100;
        StartCoroutine(KillMe());
    }

    private IEnumerator KillMe()
    {
        yield return new WaitForSeconds(0.1f);
        
        Destroy(gameObject);

    }

    private void Create(int v)
    {
        
        GameObject goMunus = Instantiate(gameObject, transform.parent);
        goMunus.GetComponent<Enemy>().MyType = Statistics.TYPE_ENEMY.PIECES;
        float scale = UnityEngine.Random.Range(0.2f, 0.6f);
        goMunus.transform.localScale = new Vector3(scale, scale, scale);
        float force = 100f * goMunus.GetComponent<Rigidbody2D>().mass;
        switch (v) {
           case 1: goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.down * force, ForceMode2D.Impulse);
                break;
            case 2:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.up * force, ForceMode2D.Impulse);
                break;
            case 3:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.left * force, ForceMode2D.Impulse);
                break;
            case 4:
                goMunus.GetComponent<Rigidbody2D>().AddRelativeForce(Vector2.right * force, ForceMode2D.Impulse);
                break;
        }
    }


    private IEnumerator Shoot()
    {
        //Pode executar até 3 tiros simultaneos
        while (true)
        {
            yield return new WaitForSeconds(shot.ShootingPeriod);
            
                Statistics.Damage = shot.Damage;
                GameObject goShooter = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                goShooter.transform.parent = transform;
                goShooter.transform.localPosition = shot.Weapon.transform.localPosition;
                goShooter.GetComponent<Rigidbody2D>().AddForce(transform.up * ((IncreasedShotSpeed(IncreasedShotSpeed(shot.SpeedShooter)) * 82000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShooter.AddComponent<BoxCollider2D>();
                goShooter.transform.parent = transform.parent;

                if (shot.TypeShooter == Statistics.TYPE_SHOT.DOUBLE)
                {
                    GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter2.transform.parent = transform;
                    goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                    goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((IncreasedShotSpeed(shot.SpeedShooter) * 82000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter2.AddComponent<BoxCollider2D>();
                    goShooter2.transform.parent = transform.parent;
                }

                if (shot.TypeShooter == Statistics.TYPE_SHOT.TRIPLE)
                {
                    GameObject goShooter2 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter2.transform.parent = transform;
                    goShooter2.transform.localPosition = shot.Weapon2.transform.localPosition;
                    goShooter2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((IncreasedShotSpeed(shot.SpeedShooter) * 82000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter2.AddComponent<BoxCollider2D>();
                    goShooter2.transform.parent = transform.parent;

                    GameObject goShooter3 = Instantiate(shot.Prefab, Vector3.zero, Quaternion.identity);
                    goShooter3.transform.parent = transform;
                    goShooter3.transform.localPosition = shot.Weapon3.transform.localPosition;
                    goShooter3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((IncreasedShotSpeed(shot.SpeedShooter) * 82000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShooter3.AddComponent<BoxCollider2D>();
                    goShooter3.transform.parent = transform.parent;
                }
        }
    }

    public void EndBoss()
    {
        GameObject.Find("Control").GetComponent<ControlGame>().LevelPassed();
        Debug.Log("BossDefeated");
        Destroy(gameObject);

    }


    public float IncreasedShotSpeed(float _shotSpeed)
    {
        return _shotSpeed = UnityEngine.Random.Range(10.0f, 20.0f);
    }
}
