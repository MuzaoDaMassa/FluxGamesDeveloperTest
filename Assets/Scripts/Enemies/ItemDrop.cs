﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*este scipt vai anexado no objeto que é dropado, ele executa a função quando o jogador encosta nele*/
public class ItemDrop : MonoBehaviour {
    public Statistics.TYPE_DROP DropType;
    //public GameObject _player;

    private void OnTriggerEnter2D(Collider2D objeto)
    {
        if (objeto.gameObject.tag == "Player")
        {
            switch (DropType)
            {
                case Statistics.TYPE_DROP.SHIELD:
                    Statistics.WithShield = true;
                    break;
                case Statistics.TYPE_DROP.SHOT1:
                    Statistics.ShootingSelected = 0;
                    break;
                case Statistics.TYPE_DROP.SHOT2:
                    Statistics.ShootingSelected = 1;
                    break;
                case Statistics.TYPE_DROP.SHOT3:
                    Statistics.ShootingSelected = 2;
                    break;
                case Statistics.TYPE_DROP.LIFEFULL:
                    objeto.gameObject.GetComponent<Life>().life = 10;
                    break;
            }
            Destroy(gameObject);
        }
        
    }
}
