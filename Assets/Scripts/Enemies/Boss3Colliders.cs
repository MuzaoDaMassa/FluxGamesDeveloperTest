using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss3Colliders : MonoBehaviour
{
    public Boss3 _boss3;
    public Life _lifeScript;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Shot")
        {
            Destroy(other.gameObject);
            if (_lifeScript.life > 0)
            {
                _lifeScript.TakesLife(1);
            }
            else
            {
                _boss3.KillMe(this.gameObject);
            }
            
        }
    }
}
