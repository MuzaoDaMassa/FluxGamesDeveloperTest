﻿using Gaminho;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CallScene : MonoBehaviour {


    public void Call(string sname)
    {
        GameObject.Instantiate(Resources.Load(Statistics.PREFAB_LOAD) as GameObject);
        SceneManager.LoadScene(sname);
    }

    public void ReturnToMenuAndSave(string _sceneName)
    {
        GameObject.Instantiate(Resources.Load(Statistics.PREFAB_LOAD) as GameObject);
        SceneManager.LoadScene(_sceneName);

        SaveSystem.SavePlayer(GameObject.Find("Control").GetComponent<ControlGame>());
    }
}
