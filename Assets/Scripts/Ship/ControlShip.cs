﻿using UnityEngine;
using System.Collections;
using System;
using Gaminho;

public class ControlShip : MonoBehaviour
{
    #region Public

    public float Velocity  = 10f;
    public float _speedRotation = 200.0f;
    public bool _ghostPlayer;
    public float timeRemaining;
    public Life life;
    public Shot[] Shots;
    public GameObject MotorAnimation;
    public GameObject Shield;
    public GameObject Explosion;
    public ShieldHealthBar _shieldHealthBar;
    public ControlGame controlGame;   
    #endregion

    #region Private
    private Vector3 startPos;
    private bool shooting = false;
    private int _shieldHealth;

    public bool getShooting() {
        return shooting;
    }

    public int getLifeShield(){
        return _shieldHealth;
    }

    public Vector3 getStartPos() {
        return startPos;
    }


    #endregion

    void Start()
    {
        timeRemaining = 60 * Time.deltaTime;

        if (_ghostPlayer)
        {
            _speedRotation = 100.0f;
        }

        startPos = transform.localPosition;
        Statistics.Player = gameObject.transform;

        if (GetComponent<Rigidbody2D>() == null)
        {
            Debug.LogError("Component required Rigidbody2D");
            Destroy(this);
            return;
        }

        if (GetComponent<BoxCollider2D>() == null)
        {
            Debug.LogWarning("BoxCollider2D not found, adding ...");
            gameObject.AddComponent<BoxCollider2D>();
            
        }
        
        GetComponent<Rigidbody2D>().gravityScale = 0.001f;
        StartCoroutine(Shoot());
    }
    
    void LateUpdate()
    {
        #region Move
        float rotation = Input.GetAxis("Horizontal") * _speedRotation;
        rotation *= Time.deltaTime;
        transform.Rotate(0, 0, -rotation);


        if (Input.GetAxis("Vertical") != 0)
        {
            Vector2 translation = Input.GetAxis("Vertical") * new Vector2(0, Velocity * GetComponent<Rigidbody2D>().mass); 
            translation *= Time.deltaTime;
            GetComponent<Rigidbody2D>().AddRelativeForce(translation, ForceMode2D.Impulse);
        }
        AnimateMotor();

        if(transform.localPosition.y > controlGame.ScenarioLimit.yMax || transform.localPosition.y < controlGame.ScenarioLimit.yMin || transform.localPosition.x > controlGame.ScenarioLimit.xMax || transform.localPosition.x < controlGame.ScenarioLimit.xMin)
        {
            Vector3 dir = startPos - transform.localPosition;
            dir = dir.normalized;
            GetComponent<Rigidbody2D>().AddForce(dir * (2 * GetComponent<Rigidbody2D>().mass), ForceMode2D.Impulse);
            
        }
        #endregion

        #region Tiro
        if(_ghostPlayer)
        {
            timeRemaining -= 1;
            if(timeRemaining < 0)
            {
                shooting = true;
            }
            else
            {
                var _ghostFireRate = UnityEngine.Random.Range(40, 80);
                timeRemaining = _ghostFireRate * Time.deltaTime;
                shooting = false;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                shooting = true;
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                shooting = false;
            }
        }

        #endregion

        Statistics.Life = life.life;
    }

    private void AnimateMotor()
    {
        if(MotorAnimation.activeSelf != (Input.GetAxis("Vertical") != 0))
        {
            MotorAnimation.SetActive(Input.GetAxis("Vertical") != 0);
        }
    }


    private IEnumerator Shoot()
    {
        while (true)
        {
            yield return new WaitForSeconds(Shots[Statistics.ShootingSelected].ShootingPeriod);
            if (shooting)
            {
                Statistics.Damage = Shots[Statistics.ShootingSelected].Damage;
                GameObject goShoot = Instantiate(Shots[Statistics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                goShoot.transform.parent = transform;
                goShoot.transform.localPosition = Shots[Statistics.ShootingSelected].Weapon.transform.localPosition;
                goShoot.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statistics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                goShoot.AddComponent<BoxCollider2D>();
                goShoot.transform.parent = transform.parent;

                if(Shots[Statistics.ShootingSelected].TypeShooter == Statistics.TYPE_SHOT.DOUBLE)
                {
                    GameObject goShoot2 = Instantiate(Shots[Statistics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot2.transform.parent = transform;
                    goShoot2.transform.localPosition = Shots[Statistics.ShootingSelected].Weapon2.transform.localPosition;
                    goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statistics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot2.AddComponent<BoxCollider2D>();
                    goShoot2.transform.parent = transform.parent;
                }

                if (Shots[Statistics.ShootingSelected].TypeShooter == Statistics.TYPE_SHOT.TRIPLE)
                {
                    GameObject goShoot2 = Instantiate(Shots[Statistics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goShoot2.transform.parent = transform;
                    goShoot2.transform.localPosition = Shots[Statistics.ShootingSelected].Weapon2.transform.localPosition;
                    goShoot2.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statistics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goShoot2.AddComponent<BoxCollider2D>();
                    goShoot2.transform.parent = transform.parent;

                    GameObject goTiro3 = Instantiate(Shots[Statistics.ShootingSelected].Prefab, Vector3.zero, Quaternion.identity);
                    goTiro3.transform.parent = transform;
                    goTiro3.transform.localPosition = Shots[Statistics.ShootingSelected].Weapon3.transform.localPosition;
                    goTiro3.GetComponent<Rigidbody2D>().AddForce(transform.up * ((Shots[Statistics.ShootingSelected].SpeedShooter * 12000f) * Time.deltaTime), ForceMode2D.Impulse);
                    goTiro3.AddComponent<BoxCollider2D>();
                    goTiro3.transform.parent = transform.parent;
                }
            }

            CallShield();
        }
    }

    private void CallShield()
    {
        
        if(Shield.activeSelf != Statistics.WithShield)
        {
            Shield.SetActive(Statistics.WithShield);
            _shieldHealth = 4;
            _shieldHealthBar.SetMaxShieldHealth(_shieldHealth);
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
       
        if (other.gameObject.tag == "Enemy")
        {
            Instantiate(Explosion, transform);
            
            if (Statistics.WithShield)
            {
                _shieldHealth -= other.gameObject.GetComponent<Enemy>().Damage;
                _shieldHealthBar.SetShieldHealth(_shieldHealth);
                Debug.Log(_shieldHealth);
            }
            else
            {
                StartCoroutine(life.TakesLife(other.gameObject.GetComponent<Enemy>().Damage));
            }

        }

        if ( other.gameObject.tag == "Shot" || other.gameObject.tag == "EnemyShot")
        {
            Instantiate(Explosion, transform);
 
            if (Statistics.WithShield)
            {
                _shieldHealth--;
                _shieldHealthBar.SetShieldHealth(_shieldHealth);
                Debug.Log(_shieldHealth);
            }
            else
            {
                StartCoroutine(life.TakesLife(1));
            }

            Destroy(other.gameObject);
        }
        
        if(_shieldHealth <= 0)
        {
            Statistics.WithShield = false;
        }
    }
   
}


