using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldHealthBar : MonoBehaviour
{
    public Slider _slider;

    public void SetMaxShieldHealth(int _health)
    {
        _slider.maxValue = _health;
        _slider.value = _health;
    }

    public void SetShieldHealth(int _health)
    {
        _slider.value = _health;
    }
}
